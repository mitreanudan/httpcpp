﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Threading.Tasks;

namespace httpcpp
{
    public class HttpServer
    {
        public delegate void ResponseHandler(HttpListenerContext request);

        private readonly HttpListener _listener;
        private readonly string _protocol = "http";
        private readonly string _prefix;
        private readonly ResponseHandler _handler;

        private string _host = "*";
        private bool _shutdown = false;

        public int Port { get; private set; }
        public string Host { get => _host; }
        public ulong RequestCount { get; private set; }
        public string Prefix { get => _prefix; }

        public HttpServer(ResponseHandler responseHandler, int port = 8080, string protocol = "http", string host = "*")
        {
            Port = port;
            _protocol = protocol;
            _host = host;
            _handler = responseHandler;

            _prefix = _protocol + "://" + _host + ":" + Port.ToString() + "/";

            Console.WriteLine("Creating HttpServer at {0}.", _prefix);

            try
            {
                _listener = new HttpListener();
                _listener.Prefixes.Add(_prefix);
                Console.WriteLine("HttpServer created.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Could not create HttpServer at {0} :", _prefix);
                Console.WriteLine(ex.Message);
            }
        }

        public void Start()
        {
            try
            {
                StartAsync().Wait();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Could not start HttpServer at {0}.", _prefix);
                Console.WriteLine(ex.Message);
            }
        }

        public void Stop()
        {
            if (!_shutdown)
            {
                Console.WriteLine("Stopping HttpServer at {0}.", _prefix);
                _listener.Stop();
                _shutdown = true;
            }
        }

        public async Task StartAsync()
        {
            Console.WriteLine("Starting HttpServer at {0}.", _prefix);

            _listener.Start();

            while (!_shutdown)
            {
                var context = await _listener.GetContextAsync();
                RequestCount++;

                _ = Task.Run(() =>
                {
                    _handler?.Invoke(context);
                });
            }
        }
    }
}
