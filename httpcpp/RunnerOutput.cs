﻿using System;
using System.Collections.Generic;
using System.Text;

namespace httpcpp
{
    public class RunnerOutput
    {
        public bool CompileSuccess { get; set; }
        public bool RuntimeSuccess { get; set; }
        public string ExecName { get; set; }
        public string CppName { get; set; }
        public string CompilerError { get; set; }
        public string ElapsedTime { get; set; }
        public bool TimeExceeded { get; set; }
        public string RuntimeOutput { get; set; }
        public string RuntimeError { get; set; }
    }
}
