﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace httpcpp
{
    public class CppWorker
    {
        public static CppWorker Instance;

        public enum OsType { Linux, MacOs, Windows }

        private readonly string _cmdPattern;
        private readonly OsType _osType;

        private string _codePath { get; set; } = null;

        public string CompilerPath { get; private set; }
        public string BuildPath { get; private set; }
        public string CodePath
        {
            get => string.IsNullOrEmpty(_codePath) ? BuildPath : _codePath;
            set => _codePath = value;
        }
        public string BuildOptions { get; set; }
        public bool DestroyFile { get; set; } = true;
        public bool UseWindowsExtension { get; set; }
        public int ActiveFiles { get; private set; }
        public float MaxProcessTime { get; set; } = 1;
        public long MaxProcessMemory { get; set; } = 1024 * 1024;
        public bool ProcessAsAdmin { get; set; } = false;
        public bool CompilerAsAdmin { get; set; } = false;

        public CppWorker(string compilerPath, string build, OsType? os, string? pattern = null)
        {
            CompilerPath = compilerPath;
            BuildPath = build;
            _cmdPattern = string.IsNullOrEmpty(pattern) ? "{opt} {in} -o {out}" : pattern;
            _osType = os.HasValue ? os.Value : OsType.Linux;

            if (_osType == OsType.Windows)
                UseWindowsExtension = true;
        }

        public string Compile(string cppName, string buildName, string? buildOptions = null)
        {
            string compilerOutput = null;

            buildOptions = string.IsNullOrEmpty(buildOptions) ? BuildOptions : buildOptions;

            string cppPath = Path.Combine(CodePath, cppName);
            string execPath = Path.Combine(BuildPath, buildName);

            string arguments = _cmdPattern
                .Replace("{opt}", buildOptions)
                .Replace("{in}", "\"" + cppPath + "\"")
                .Replace("{out}", "\"" + execPath + "\"");

            var process = new Process()
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = CompilerPath,
                    Arguments = arguments,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false,
                    CreateNoWindow = true
                }
            };
            if (CompilerAsAdmin)
                process.StartInfo.Verb = "runas";
            try
            {
                process.Start();
                compilerOutput = "";
                compilerOutput = process.StandardError.ReadToEnd();
                process.WaitForExit();
            } catch (Exception ex)
            {
                Console.WriteLine("Error: Compilation not possible. Check compiler command / path.");
                Console.WriteLine(ex.Message);
            }
            return compilerOutput;
        }

        public RunnerOutput CreateAndRun(string code, string? input = null, string? cppName = null, string? buildName = null, string? stdin = null, string? buildOptions = null)
        {
            string guid = Guid.NewGuid().ToString();
            cppName = string.IsNullOrEmpty(cppName) ? guid : cppName;
            cppName = cppName.EndsWith(".cpp") ? cppName : cppName + ".cpp";

            buildName = string.IsNullOrEmpty(buildName) ? guid : buildName;
            if (UseWindowsExtension)
                buildName = buildName.EndsWith(".exe") ? buildName : buildName + ".exe";

            string cppPath = Path.Combine(CodePath, cppName);
            string execPath = Path.Combine(BuildPath, buildName);

            File.WriteAllText(cppPath, code, Encoding.ASCII);
            ActiveFiles++;

            string compilerOutput = Compile(cppName, buildName, buildOptions);
            string execOutput = "", execError = "";

            bool compilationSuccess = File.Exists(execPath), runtimeSuccess = true;
            if (compilationSuccess) ActiveFiles++;

            long elapsedRuntime = 0;
            bool timeExceeded = false;

            if (compilationSuccess)
            {
                var process = new Process()
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = execPath,
                        RedirectStandardInput = true,
                        RedirectStandardOutput = true,
                        RedirectStandardError = true,
                        UseShellExecute = false,
                        CreateNoWindow = true,
                    }
                };
                if (ProcessAsAdmin)
                    process.StartInfo.Verb = "runas";

                Stopwatch measure = new Stopwatch();
                measure.Start();

                process.Start();
                process.MaxWorkingSet = new IntPtr(MaxProcessMemory);
                process.StandardInput.NewLine = "\n";

                if (!string.IsNullOrEmpty(input))
                {
                    if (!input.EndsWith("\n"))
                        input += "\n";
                    foreach (char c in input)
                        process.StandardInput.Write(c);
                }

                using (var timer = new Timer(
                    delegate
                    { 
                        process.Kill();
                        runtimeSuccess = false;
                        timeExceeded = true;
                    },
                    null,
                    (int)(MaxProcessTime * 1000),
                    Timeout.Infinite))
                {
                    execOutput = process.StandardOutput.ReadToEnd();
                    execError = process.StandardError.ReadToEnd();
                    process.WaitForExit();
                }

                measure.Stop();
                elapsedRuntime = measure.ElapsedMilliseconds;
            }

            RunnerOutput output = new RunnerOutput
            {
                CompileSuccess = compilationSuccess,
                CompilerError = compilerOutput,
                ExecName = buildName,
                CppName = cppName,
                RuntimeSuccess = runtimeSuccess,
                ElapsedTime = elapsedRuntime.ToString() + "ms",
                TimeExceeded = timeExceeded,
                RuntimeOutput = execOutput,
                RuntimeError = execError
            };

            if (DestroyFile)
            {
                File.Delete(cppPath);
                ActiveFiles--;
                File.Delete(execPath);
                ActiveFiles--;
            }

            return output;
        }
    }
}
