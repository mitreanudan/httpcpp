﻿using System;
using System.Net;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;
using System.Runtime.InteropServices;

namespace httpcpp
{
    class Program
    {
        private static bool HowToOnce = true;

        static int Main(string[] args)
        {
            Console.WriteLine("Reading configuration...");
            if (!File.Exists("config.json"))
            {
                Console.WriteLine("Configuration not found. Creating...");
                byte[] defaultConfig = (byte[])Properties.Resources.ResourceManager.GetObject("config");
                string configStr = System.Text.Encoding.ASCII.GetString(defaultConfig);
                File.WriteAllText("config.json", configStr);
            }

            string configText = File.ReadAllText("config.json");
            Console.WriteLine(configText);
            Config config = JsonConvert.DeserializeObject<Config>(configText);

            HttpServer server = new HttpServer(
                RequestHandler,
                host: config.HttpHost,
                port: config.HttpPort,
                protocol: config.HttpProtocol);

            if (server == null)
                return 0xE1;

            CppWorker.Instance = new CppWorker(
                compilerPath: config.Compiler,
                build: config.BuildPath,
                os: config.OperatingSystem,
                pattern: config.CommandPattern);
            CppWorker.Instance.CodePath = config.CodePath;
            CppWorker.Instance.DestroyFile = config.DestroyFiles;
            CppWorker.Instance.MaxProcessTime = config.MaxProgramTime;
            CppWorker.Instance.MaxProcessMemory = config.MaxProgramMem;
            CppWorker.Instance.CompilerAsAdmin = config.CompilerAdmin;
            CppWorker.Instance.ProcessAsAdmin = config.ProgramAdmin;
            HowToOnce = config.OpenHowTo;

            var serverTask = server.StartAsync();

            if (HowToOnce)
                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                    System.Diagnostics.Process.Start("cmd", $"/c start {server.Prefix}");
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                    System.Diagnostics.Process.Start("xdg-open", server.Prefix);
                else if (RuntimeInformation.IsOSPlatform(OSPlatform.OSX))
                    System.Diagnostics.Process.Start("open", server.Prefix);

            Console.WriteLine();
            Console.Write("httpcpp> ");
            while (!serverTask.IsCompleted)
            {
                string cmd = Console.ReadLine();

                switch (cmd)
                {
                    case "quit":
                        server.Stop();
                        return 0;

                    case "exit":
                        server.Stop();
                        return 0;

                    case "req count":
                        Console.WriteLine(server.RequestCount);
                        break;

                    case "file count":
                        Console.WriteLine(CppWorker.Instance.ActiveFiles);
                        break;

                    case "config rewrite":
                        Console.WriteLine("Rewriting config...");
                        byte[] defaultConfig = (byte[])Properties.Resources.ResourceManager.GetObject("config");
                        string configStr = System.Text.Encoding.ASCII.GetString(defaultConfig);
                        File.WriteAllText("config.json", configStr);
                        Console.WriteLine("Config rewritten.");
                        break;
                }

                Console.Write("httpcpp> ");
            }

            return 0x0;
        }

        static void RequestHandler(HttpListenerContext context)
        {
            if (HowToOnce)
            {
                context.Response.AddHeader("Access-Control-Allow-Origin", "*");
                string howtoFile = (string)Properties.Resources.ResourceManager.GetObject("howto");
                context.Response.OutputStream.Write(System.Text.Encoding.ASCII.GetBytes(howtoFile));
                context.Response.Close();
                HowToOnce = false;
                return;
            }

            if (context.Request.HttpMethod != "post" && context.Request.HttpMethod != "POST")
            {
                // method not post => 405 method not allowed
                context.Response.StatusCode = 405;
                context.Response.Close();
                return;
            }

            if (!context.Request.HasEntityBody)
            {
                // no request body => 400 bad request
                context.Response.StatusCode = 400;
                context.Response.Close();
                return;
            }

            if (context.Request.ContentType != "text/plain")
            {
                // cpp trb servit ca text/plain (ASCII), altfel 400 bad request
                context.Response.StatusCode = 400;
                context.Response.AddHeader("Accept", "text/plain");
                context.Response.Close();
                return;
            }

            string request, code = null, input = null;
            using (Stream body = context.Request.InputStream)
            using (StreamReader reader = new StreamReader(body, System.Text.Encoding.ASCII))
                request = reader.ReadToEnd();

            if (request.StartsWith("<!--"))
            {
                if (!request.Contains("-->"))
                {
                    context.Response.StatusCode = 400;
                    context.Response.Close();
                    return;
                }

                int codeI = 0;
                string[] lines = request.Split("\n");
                for (int i = 1; i < lines.Length && lines[i] != "-->"; codeI = ++i)
                    input += lines[i] + '\n';

                for (int i = codeI + 1; i < lines.Length; i++)
                    code += lines[i] + '\n';
            }
            else
                code = request;

            if (string.IsNullOrEmpty(code))
            {
                context.Response.StatusCode = 400;
                context.Response.Close();
                return;
            }

            var result = CppWorker.Instance.CreateAndRun(code, input);
            string resultText = JsonConvert.SerializeObject(result);
            var resultBytes = System.Text.Encoding.UTF8.GetBytes(resultText);

            context.Response.OutputStream.Write(resultBytes);

            context.Response.Close();
        }
    }
}
