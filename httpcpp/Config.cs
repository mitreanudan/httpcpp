﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace httpcpp
{
    public class Config
    {
        [JsonProperty("httpPort")]
        public int HttpPort { get; set; } = 8080;
        [JsonProperty("httpProtocol")]
        public string HttpProtocol { get; set; } = "http";
        [JsonProperty("httpHost")]
        public string HttpHost { get; set; } = "localhost";
        [JsonProperty("openHowTo")]
        public bool OpenHowTo { get; set; } = false;
        [JsonProperty("compiler")]
        public string Compiler { get; set; } = "g++";
        [JsonProperty("osType")]
        public string OsType { get; set; } = "linux";
        [JsonProperty("buildPath")]
        public string BuildPath { get; set; } = "";
        [JsonProperty("codePath")]
        public string CodePath { get; set; } = "";
        [JsonProperty("destroyFiles")]
        public bool DestroyFiles { get; set; } = true;
        [JsonProperty("compilePattern")]
        public string CommandPattern { get; set; } = "{opt} {in} -o {out}";
        [JsonProperty("compilerAdmin")]
        public bool CompilerAdmin { get; set; } = false;
        [JsonProperty("programAdmin")]
        public bool ProgramAdmin { get; set; } = false;
        [JsonProperty("maxProgramTime")]
        public float MaxProgramTime { get; set; } = 1.5f;
        [JsonProperty("maxProgramMem")]
        public long MaxProgramMem { get; set; } = 5242880;

        [JsonIgnore]
        public CppWorker.OsType OperatingSystem
        {
            get
            {
                switch (OsType)
                {
                    case "windows":
                        return CppWorker.OsType.Windows;
                    case "linux":
                        return CppWorker.OsType.Linux;
                    case "osx":
                        return CppWorker.OsType.MacOs;
                    default:
                        return CppWorker.OsType.Linux;
                }
            }
        }
    }
}
